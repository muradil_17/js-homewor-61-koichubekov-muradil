import React, {Component} from 'react';
import axios from 'axios';
import '../style/style.css'

class Country extends Component {
    constructor(props){
        super(props);

        this.state = {
            Document: null,
            country : []
        }
    }


    componentDidMount() {
        const allsCountry = 'https://restcountries.eu/rest/v2/all';
        axios.get(allsCountry).then(response => {
            this.setState({country: response.data});
        });
    }


    searchCountry = event =>{
        event.preventDefault();
        const UserCountrySearch = 'https://restcountries.eu/rest/v2/name/';
        axios.get(`${UserCountrySearch}${event.target.value}`).then(response => {
            this.setState ({Document: response.data[0]});
        })
    };

    render() {
        const {country} = this.state;
        var countryCatalog = country.length > 0
            && country.map((array, j) => {
                return (
                    <option key={j} value={array.id}>{array.name}</option>
                )
            }, this);
        return (
            <div className="content">
                <form onSubmit={this.searchCountry}>
                    <input type="text" name="country" placeholder="Введите страну" onChange={(event)=>this.searchCountry(event)} />
                    <button>search</button>
                    <select onChange={(event)=>this.searchCountry(event)} >{countryCatalog}</select>
                </form>
                <div className="result">
                    <div className="Flag" >{this.state.Document? <img src={this.state.Document.flag}  alt="flag"/> :null}</div>
                    <div className="Document"> Страна: {this.state.Document? this.state.Document.name:null}</div>
                    <div className="Document"> Столица: {this.state.Document? this.state.Document.capital:null}</div>
                    <div className="Document"> Официальные название: {this.state.Document&& this.state.Document.altSpellings.join(' , ')}</div>
                    <div className="Document">Регион: {this.state.Document? this.state.Document.region:null}</div>
                    <div className="Document">Население: {this.state.Document? this.state.Document.population:null}</div>
                    <div className="Document">Граничит с: {this.state.Document && this.state.Document.borders.join(" ,")}</div>
                </div>
            </div>
        );
    }
}

export default Country;